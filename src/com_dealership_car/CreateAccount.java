package com_dealership_car;

import java.util.Scanner;

public class CreateAccount {
	
	public CreateAccount() {
		System.out.println("In order to create an account Follow the steps below: ");
	}
	
	//method called if account being created is customer
	public static  void getUserInfo() {
		Scanner inputCustomer = new Scanner(System.in);
		
		String name; String age; String address; String userName; String password;
	
		System.out.print("Enter first and last name: ");name= inputCustomer.nextLine();
		System.out.print("Enter your age: ");age = inputCustomer.nextLine();
		System.out.print("Enter your address: ");address = inputCustomer.nextLine();
		System.out.print("Create user name: ");userName =inputCustomer.nextLine();
		System.out.print("Create a password: ");password = inputCustomer.nextLine();
	
	}
	
	//method called if account being created is employee
	public static  void getEmplyeeInfo() {
	Scanner inputEmployee = new Scanner(System.in);
	
	String name; String age; String address; String userName; String password;

	System.out.print("Enter first and last name: ");name= inputEmployee.nextLine();
	System.out.print("Enter your age: ");age = inputEmployee.nextLine();
	System.out.print("Enter your address: ");address = inputEmployee.nextLine();
	System.out.print("Create user name: ");userName =inputEmployee.nextLine();
	System.out.print("Create a password: ");password = inputEmployee.nextLine();
	
	}
	
	public static void main(String[] args) {
		Scanner inquireStat = new Scanner(System.in);
		
		String inquire;
	
		System.out.print("Are you a customer or employee: ");inquire= inquireStat.nextLine();
		
		// is this a customer or employee
		if (inquire.equals("employee")) {
			getEmplyeeInfo();
		}
		else if(inquire.equals("customer")) {
			getUserInfo();
		}
	}
}
