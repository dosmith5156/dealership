package com_vehicle;

public interface Specs {

	int vin();
	String make();
	String color();
	String carModel();
	boolean offer();
	int price();
	int makeOffer();
	int year();
		
	}
	
